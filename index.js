'use strict'

function getHost(network) {
    switch (network) {
        case 'mainnet': return 'https://api.etherscan.io';
        case 'goerli': return 'https://api-goerli.etherscan.io'; 
        case 'kovan': return 'https://api-kovan.etherscan.io';
        case 'ropsten': return 'https://api-ropsten.etherscan.io';
        case 'rinkeby': return 'https://api-rinkeby.etherscan.io';
        case 'sepolia': return 'https://api-sepolia.etherscan.io';
        default: return 'https://api.etherscan.io';
    }
}

module.exports = function (config) {
    this.web3 = require('web3')
    config.host = getHost(config.network);
    // console.log(`Running on: ${config.host}`)

    this.accounts = require('./src/accounts')(config)
    this.contracts = require('./src/contracts')(config)
    this.transactions = require('./src/transactions')(config)
    this.blocks = require('./src/blocks')(config)
    this.logs = require('./src/logs')(config)
    this.proxy = require('./src/proxy')(config)
    this.tokens = require('./src/tokens')(config)
    this.gastracker = require('./src/gastracker')(config)
    this.stats = require('./src/stats')(config)
};
