
# etherscan

node js etherscan api


#### api

[https://docs.etherscan.io/](https://docs.etherscan.io/)

```
- Accounts
    getAccountBalance,
    getAccountBalances,
    getTransactions,
    getInternalTransactions,
    getInternalTransactionsByHash,
    getInternalTransactionsByBlockRange,
    getERC20TokenTransferEventsByAddress,
    getERC721TokenTransferEventsByAddress,
    getERC1155TokenTransferEventsByAddress,
    getMinedBlocks,

- Contracts
    getContractAbi,
    getContractSource,
    getContractCreator,

- Transactions
    getContractExecutionStatus,
    getTransactionStatus,

- Blocks
    getBlockReward,
    getBlockCountDownTime,
    getBlockByTimestamp,

- Logs
    getEventsLogs,

- Proxy
    getBlockNumber,
    getBlockByNumber,
    getUncleByBlockNumberAndIndex,
    getBlockTransactionCount,
    getTransactionByHash,
    getTransactionByBlockNumberAndIndex,
    getTransactionCount,
    sendRawTransaction,
    getTransactionReceipt,
    call,
    getCode,
    getStorageAt,
    getGasPrice,
    estimateGas,

- Tokens
    getTokenSupplyByContractAddress,
    getTokenBalanceByContractAddress,

- GasTracker
    getConfirmationTime,
    getGasOracle,

- Stats
    getTotalEtherSupply,
    getEtherLastPrice,
    getNodeCount,
```
