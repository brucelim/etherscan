require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the Contract Application Binary Interface ( ABI ) of a verified smart contract.
 * @param address
 * @returns {Promsie<object[]>}
 */
const getContractAbi = (address) => {
    return rpc(host, {
        module: "contract",
        action: "getabi",
        address,
        apikey
    }).then(response => {
       return JSON.parse(response.result)
    })
}

/**
 * Returns the Solidity source code of a verified smart contract.
 * @param address
 * @returns {Promsie<object[]>}
 */
const getContractSource = (address) => {
    return rpc(host, {
        module: "contract",
        action: "getsourcecode",
        address,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns a contract's deployer address and transaction hash it was created, up to 5 at a time.
 * @param address
 * @returns {Promsie<object[]>}
 */
const getContractCreator = (addresses) => {
    const contractaddresses = typeof addresses == 'string' ?
        [addresses] : addresses;

    return rpc(host, {
        module: "contract",
        action: "getcontractcreation",
        contractaddresses: (contractaddresses || []).join(','),
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;

    return {
        getContractAbi,
        getContractSource,
        getContractCreator,
    }
}
