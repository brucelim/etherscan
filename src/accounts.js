require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the Ether balance of a given address.
 * @param {string} address
 * @param {object} [options]
 * @param {string} [options.unit=wei]
 * @param {string} [options.tag=latest]
 * @returns {Promise<string>}
 */
const getAccountBalance = (address, options = {}) => {
    const { unit='wei', tag='latest' } = options

    return rpc(host, {
        module: "account",
        action: "balance",
        address,
        tag,
        apikey
    })
    .then(response => {
        return unit === 'wei'
            ? response.result
            : web3.utils.fromWei(response.result, unit)
    });
}

/**
 * Returns the balance of the accounts from a list of addresses.
 * @param {Array<string>} addresses
 * @param {object} [options]
 * @param {string} [options.unit=wei]
 * @param {string} [options.tag=latest]
 * @return {Promise<object>}
 */
const getAccountBalances = (addresses, options = {}) => {
    const { unit='wei', tag='latest' } = options

    return rpc(host, {
        module: "account",
        action: "balancemulti",
        address: (addresses || []).join(','),
        tag,
        apikey
    }).then(response => {
      return unit === 'wei' 
        ? response.result
        : response.result.map(item => {
          return {
            account: item.account,
            balance: web3.utils.fromWei(item.balance, unit)
          }
        })
    })
}

/**
 * Returns the list of transactions performed by an address, with optional pagination.
 * @param {string} address
 * @param {object} [options]
 * @param {object} [options.startblock]
 * @param {string|number} [options.endblock]
 * @param {number} [options.offset]
 * @param {number} [options.page]
 * @param {"asc"|"desc"} [options.sort]
 * @returns {Promise<object[]>}
 */
const getTransactions = (address, options={}) => {
    const { startblock, endblock, offset, page, sort } = options

    return rpc(host, {
        module: "account",
        action: "txlist",
        address,
        startblock,
        endblock,
        offset,
        page,
        sort,
        apikey
    }).then(response => {
        return response.result;
    });
}

/**
 * Returns the list of internal transactions performed by an address, with optional pagination.
 * @param {string} address
 * @param {object} [options]
 * @param {string|number} [options.startblock]
 * @param {string|number} [options.endblock]
 * @param {string|number} [options.offset]
 * @param {string|number} [options.page]
 * @param {"asc"|"desc"} [options.sort]
 * @returns {Promise<object[]>}
 */
const getInternalTransactions = (address, options = {}) => {
    const { startblock, endblock, offset, page, sort } = options

    return rpc(host, {
        module: "account",
        action: "txlistinternal",
        address,
        startblock,
        endblock,
        offset,
        page,
        sort,
        apikey
    }).then(response => {
        return response.result
    })
}

/**
 * Returns the list of internal transactions performed within a transaction.
 * @param txhash
 * @returns {Promise<object[]>}
 */
const getInternalTransactionsByHash = (txhash) => {
    return rpc(host, {
        module: "account",
        action: "txlistinternal",
        txhash,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the list of internal transactions performed within a block range, with optional pagination.
 * @param {object} [options]
 * @param {string|number} [options.startblock]
 * @param {string|number} [options.endblock]
 * @param {string|number} [options.offset]
 * @param {string|number} [options.page]
 * @param {"asc"|"desc"} [options.sort]
 * @returns {Promise<object[]>}
 */
const getInternalTransactionsByBlockRange = (options={}) => {
    const { startblock, endblock, offset, page, sort } = options

    return rpc(host, {
        module: "account",
        action: "txlistinternal",
        startblock,
        endblock,
        offset,
        page,
        sort,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the list of ERC-20 tokens transferred by an address, with optional filtering by token contract.
 * @param {string} address
 * @param {string} contract
 * @param {object} [options]
 * @param {string|number} [options.startblock]
 * @param {string|number} [options.endblock]
 * @param {string|number} [options.offset]
 * @param {string|number} [options.page]
 * @param {"asc"|"desc"} [options.sort]
 * @returns {Promise<object[]>}
 */
const getERC20TokenTransferEventsByAddress = (address, contract=null, options={}) => {
    const { startblock, endblock, offset, page, sort } = options

    return rpc(host, {
        module: "account",
        action: "tokentx",
        address,
        contract,
        startblock,
        endblock,
        offset,
        page,
        sort,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the list of ERC-721 ( NFT ) tokens transferred by an address, with optional filtering by token contract.
 * @param {string} address
 * @param {string} contract
 * @param {object} [options]
 * @param {string|number} [options.startblock]
 * @param {string|number} [options.endblock]
 * @param {string|number} [options.offset]
 * @param {string|number} [options.page]
 * @param {"asc"|"desc"} [options.sort]
 * @returns {Promise<object[]>}
 */
const getERC721TokenTransferEventsByAddress = (address, contract=null, options={}) => {
    const { startblock, endblock, offset, page, sort } = options

    return rpc(host, {
        module: "account",
        action: "tokennfttx",
        address,
        contract,
        startblock,
        endblock,
        offset,
        page,
        sort,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the list of ERC-1155 ( Multi Token Standard ) tokens transferred by an address, with optional filtering by token contract.
 * with optional filtering by token contract.
 * @param txhash Contract address
 * @param {string} address Contract address
 * @param {object} [options]
 */
const getERC1155TokenTransferEventsByAddress = (address, contract=null, options={}) => {
    const { startblock, endblock, offset, page, sort } = options

    return rpc(host, {
        module: "account",
        action: "token1155tx",
        address,
        contract,
        startblock,
        endblock,
        offset,
        page,
        sort,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the list of blocks mined by an address.
 * @param {string} address
 * @param {object} [options]
 * @param {"blocks"|"uncles"} [options.type] 
 * @param {number} [options.offset]
 * @param {number} [options.page]
 * @returns {Promise<object[]>}
 */
const getMinedBlocks = (address, options = {}) => {
    const { type='blocks', offset, page } = options

    return rpc(host, {
      module: "account",
      action: "getminedblocks",
      blocktype: type,
      address,
      offset,
      page,
      apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;

    return {
        getAccountBalance,
        getAccountBalances,
        getTransactions,
        getInternalTransactions,
        getInternalTransactionsByHash,
        getInternalTransactionsByBlockRange,
        getERC20TokenTransferEventsByAddress,
        getERC721TokenTransferEventsByAddress,
        getERC1155TokenTransferEventsByAddress,
        getMinedBlocks,
    }
}
