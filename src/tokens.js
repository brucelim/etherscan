require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the current amount of an ERC-20 token in circulation.
 * @param {string} contractaddress
 * @returns {Promise<string>}
 */
const getTokenSupplyByContractAddress = (contractaddress) => {
    return rpc(host, {
        module: "stats",
        action: "tokensupply",
        contractaddress,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the current balance of an ERC-20 token of an address.
 * @param {string} contractaddress the contract address of the ERC-20 token
 * @param {string} address the string representing the address to check for token balance
 * @param {object} [options]
 * @param {object} [options.tag=latest]
 * @returns {Promise<string>}
 */
const getTokenBalanceByContractAddress = (contractaddress, address, options = {}) => {
    const { tag='latest' } = options

    return rpc(host, {
        module: "account",
        action: "tokenbalance",
        contractaddress,
        address,
        tag,
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;

    return {
        getTokenSupplyByContractAddress,
        getTokenBalanceByContractAddress,
    }
}
