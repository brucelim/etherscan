require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the status code of a contract execution.
 * @param {string} txhash
 * @returns {Promise<object>}
 */
const getContractExecutionStatus = (txhash) => {
    return rpc(host, {
        module: "transaction",
        action: "getstatus",
        txhash,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the status code of a transaction execution.
 * Only applicable for post Byzantium Fork transactions.
 * @param {string} txhash
 * @returns {Promise<object>}
 */
const getTransactionStatus = (txhash) => {
    return rpc(host, {
        module: "transaction",
        action: "gettxreceiptstatus",
        txhash,
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;

    return {
        getContractExecutionStatus,
        getTransactionStatus,
    }
}
