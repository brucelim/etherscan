require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the estimated time, in seconds, for a transaction to be confirmed on the blockchain.
 * @param gasprice
 * @returns {Promsie<object[]>}
 */
const getConfirmationTime = (gasprice) => {
    return rpc(host, {
        module: "gastracker",
        action: "gasestimate",
        gasprice,
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the current Safe, Proposed and Fast gas prices.
 * @returns {Promsie<object[]>}
 */
const getGasOracle = () => {
    return rpc(host, {
        module: "gastracker",
        action: "gasoracle",
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;

    return {
        getConfirmationTime,
        getGasOracle,
    }
}
