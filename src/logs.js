require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns events logs.
 * @param {string} address
 * @param {object} options
 * @param {string|number} [options.fromBlock]
 * @param {string|number} [options.toBlock]
 * @param {string|number} [options.offset]
 * @param {string|number} [options.page]
 * @return {Promise<object>}
 */
 const getEventsLogs = (address, options = {}) => {
    const { fromBlock, toBlock, offset, page } = options

    return rpc(host, {
        module: "logs",
        action: "getLogs",
        address,
        fromBlock,
        toBlock,
        page,
        offset,
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;

    return {
        getEventsLogs,
    }
}
