require('isomorphic-fetch')
const rpc = require('./rpc')
const web3 = require('web3')


/**
 * Returns the current amount of Ether in circulation excluding ETH2 Staking rewards and EIP1559 burnt fees.
 * @returns {Promise<string>}
 */
const getTotalEtherSupply = () => {
    return rpc(host, {
        module: "stats",
        action: "ethsupply",
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the latest price of 1 ETH.
 * @returns {Promise<object>}
 */
const getEtherLastPrice = () => {
    return rpc(host, {
        module: "stats",
        action: "ethprice",
        apikey
    }).then(response => {
        return response.result;
    })
}

/**
 * Returns the total number of discoverable Ethereum nodes.
 * @param {object} [options]
 * @param {string|number} [options.startdate]
 * @param {string|number} [options.enddate]
 * @param {string|number} [options.clienttype]
 * @param {string|number} [options.syncmode]
 * @param {"asc"|"desc"} [options.sort]
 * @returns {Promise<object>}
 */
const getNodeCount = (options={}) => {
    const { startdate, enddate, clienttype, syncmode, sort } = options

    return rpc(host, {
        module: "stats",
        action: "chainsize",
        startdate,
        enddate,
        clienttype,
        syncmode,
        sort,
        apikey
    }).then(response => {
        return response.result;
    })
}


let apikey, host;
module.exports = function(config) {
    apikey = config.token;
    host = config.host;

    return {
        getTotalEtherSupply,
        getEtherLastPrice,
        getNodeCount
    }
}
