require('dotenv').config()

const test = require('tape');
const _etherscan = require('../index');
const { tokens, web3 } = new _etherscan({
    token: process.env.ETHERSCAN_APIKEY,
    network: 'mainnet'
})


test('[07|01] Get ERC20-Token TotalSupply by ContractAddress', async function(t) {
    const contractaddress = '0x57d90b64a1a57749b0f932f1a3395792e12e7055'
    const token_supply = await tokens.getTokenSupplyByContractAddress(contractaddress)

    t.ok(token_supply)
    t.is(token_supply, '21265524714464')
})

test('[07|02] Get ERC20-Token Account Balance for TokenContractAddress', async function(t) {
    const contractaddress = '0x57d90b64a1a57749b0f932f1a3395792e12e7055'
    const address = '0xe04f27eb70e025b78871a2ad7eabe85e61212761'
    const token_balance = await tokens.getTokenBalanceByContractAddress(contractaddress, address)

    t.ok(token_balance)
    t.is(token_balance, '135499')
})
