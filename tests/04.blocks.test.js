require('dotenv').config()

const test = require('tape');
const _etherscan = require('../index');
const { blocks } = new _etherscan({
    token: process.env.ETHERSCAN_APIKEY,
    network: 'mainnet'
})


test('[04|01] Get Block And Uncle Rewards by BlockNo', async function(t) {
    const reward = await blocks.getBlockReward(2165403)
    const compare = {
        blockNumber: '2165403',
        timeStamp: '1472533979',
        blockMiner: '0x13a06d3dfe21e0db5c016c03ea7d2509f7f8d1e3',
        blockReward: '5314181600000000000',
        uncles: [
          {
            miner: '0xbcdfc35b86bedf72f0cda046a3c16829a2ef41d1',
            unclePosition: '0',
            blockreward: '3750000000000000000'
          },
          {
            miner: '0x0d0c9855c722ff0c78f21e43aa275a5b8ea60dce',
            unclePosition: '1',
            blockreward: '3750000000000000000'
          }
        ],
        uncleInclusionReward: '312500000000000000'
    }

    t.ok(reward)
    t.deepEqual(reward, compare)
    t.end()
})

test('[04|02] Get Estimated Block Countdown Time by BlockNo', async function(t) {
    const countdown_time = await blocks.getBlockCountDownTime(16701588);

    t.ok(countdown_time)
    t.equal(countdown_time.CountdownBlock, '16701588')
})

test('[04|03] Get Block Number by Timestamp', async function(t) {
    const blockno = await blocks.getBlockByTimestamp(1578638524)

    t.ok(blockno)
    t.equal(blockno, '9251482')
})
