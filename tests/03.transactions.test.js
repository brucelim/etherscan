require('dotenv').config()

const test = require('tape');
const _etherscan = require('../index');
const { transactions } = new _etherscan({
    token: process.env.ETHERSCAN_APIKEY,
    network: 'mainnet'
})


test('[03|01] Check Contract Execution Status', async function(t) {
    const status = await transactions.getContractExecutionStatus('0x15f8e5ea1079d9a0bb04a4c58ae5fe7654b5b2b4463375ff7ffb490aa0032f3a')
    const compare = { isError: '1', errDescription: 'Bad jump destination' }

    t.ok(status)
    t.deepEqual(status, compare)
    t.end()
})

test('[03|02] Check Transaction Receipt Status', async function(t) {
    const status = await transactions.getTransactionStatus('0x513c1ba0bebf66436b5fed86ab668452b7805593c05073eb2d51d3a52f480a76')
    const compare = { status: '1' }

    t.ok(status)
    t.deepEqual(status, compare)
    t.end()
})
