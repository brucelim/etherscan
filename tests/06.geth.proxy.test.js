require('dotenv').config()

const test = require('tape');
const _etherscan = require('../index');
const { proxy, web3 } = new _etherscan({
    token: process.env.ETHERSCAN_APIKEY,
    network: 'mainnet'
})


test('[06|01] Proxy eth_blockNumber', async function(t) {
    const blockno = await proxy.getBlockNumber()

    t.ok(blockno)
    t.is(web3.utils.isHexStrict(blockno), true)
    t.is(typeof web3.utils.hexToNumber(blockno), 'number')
})

test('[06|02] Proxy eth_getBlockByNumber', async function(t) {
    const blockno = '0x10d4f'
    const number = web3.utils.hexToNumber(blockno)
    const block = await proxy.getBlockByNumber(number)

    t.ok(block)
    t.is(block.transactions.length, 1)
    t.is(block.number, blockno)
})

test('[06|03] eth_getUncleByBlockNumberAndIndex', async function(t) {
    const blockno = '0xC63276'
    const index = '0x0'
    const uncle = await proxy.getUncleByBlockNumberAndIndex(
        web3.utils.hexToNumber(blockno),
        web3.utils.hexToNumber(index),
    )

    t.ok(uncle);
    t.is(uncle.hash, '0x1da88e3581315d009f1cb600bf06f509cd27a68cb3d6437bda8698d04089f14a')
})

test('[06|04] eth_getBlockTransactionCountByNumber', async function(t) {
    const blockno = '0x10FB78'
    const number = web3.utils.hexToNumber(blockno)
    const transaction_count = await proxy.getBlockTransactionCount(number)

    t.ok(transaction_count)
    t.is(web3.utils.isHexStrict(transaction_count), true)
    t.is(web3.utils.hexToNumber(transaction_count), 3)
})

test('[06|05] Proxy eth_getTransactionByHash', async function(t) {
    const txhash = '0xbc78ab8a9e9a0bca7d0321a27b2c03addeae08ba81ea98b03cd3dd237eabed44'
    const transaction = await proxy.getTransactionByHash(txhash)

    t.ok(transaction)
    t.is(transaction.blockNumber, '0xcf2420')
    t.is(transaction.hash, txhash)
})

test('[06|06] Proxy eth_getTransactionByBlockNumberAndIndex', async function(t) {
    const blockno = '0xC6331D'
    const index = '0x11A'
    const transaction = await proxy.getTransactionByBlockNumberAndIndex(
        web3.utils.hexToNumber(blockno),
        web3.utils.hexToNumber(index),
    )

    t.ok(transaction)
    t.is(transaction.blockNumber, blockno.toLowerCase())
})

test('[06|07] Proxy eth_getTransactionCount', async function(t) {
    const address = '0x4bd5900Cb274ef15b153066D736bf3e83A9ba44e'
    const transaction_count = await proxy.getTransactionCount(address)

    t.ok(transaction_count)
    t.is(web3.utils.isHexStrict(transaction_count), true)
})

test('[06|08] Proxy eth_sendRawTransaction', async function(t) {
    const result = await proxy.sendRawTransaction('transaction hexcode')
    const compare = { msg: 'not implemented yet' }

    t.deepEqual(result, compare)
})

test('[06|09] Proxy eth_getTransactionReceipt', async function(t) {
    const txhash = '0xadb8aec59e80db99811ac4a0235efa3e45da32928bcff557998552250fa672eb'
    const receipt = await proxy.getTransactionReceipt(txhash)

    t.ok(receipt)
    t.is(receipt.transactionHash, txhash)
})

test('[06|10] Proxy eth_call', async function(t) {
    const to = '0xAEEF46DB4855E25702F8237E8f403FddcaF931C0'
    const data = '0x70a08231000000000000000000000000e16359506c028e51f16be38986ec5746251e9724'
    const result = await proxy.call(to, data)

    t.ok(result)
    t.is(web3.utils.isHexStrict(result), true)
})

/**
 * @TODO better assertions
 * code comes back as hex string
 */
test('[06|11] Proxy eth_getCode', async function(t) {
    const address = '0xf75e354c5edc8efed9b59ee9f67a80845ade7d0c'
    const code = await proxy.getCode(address)

    t.ok(code)
})

/**
 * @TODO better assertions
 * msg comes back as hex string
 */
test('[06|10] Proxy eth_getStorageAt', async function(t) {
    const address = '0x0e09fabb73bd3ade0a17ecc321fd13a19e81ce82'
    const position = '0x0'
    const msg = await proxy.getStorageAt(address, position)

    t.ok(msg)
})

test('[06|11] Proxy eth_gasPrice', async function(t) {
    const gasprice = await proxy.getGasPrice();

    t.ok(gasprice)
    t.is(web3.utils.isHexStrict(gasprice), true)
})

test('[06|12] Proxy eth_estimateGas', async function(t) {
    const to = '0xf0160428a8552ac9bb7e050d90eeade4ddd52843'
    const data = '0x4e71d92d'
    const value = '0xff22'
    const gasPrice = '0x51da038cc'
    const gas = '0x5f5e0ff'
    const estimate = await proxy.estimateGas(to, { data, value, gasPrice, gas })

    t.ok(estimate)
    t.is(web3.utils.isHexStrict(estimate), true)
    t.is(web3.utils.hexToNumber(estimate), 25942)
})
