require('dotenv').config()

const test = require('tape');
const _etherscan = require('../index');
const { stats, web3 } = new _etherscan({
    token: process.env.ETHERSCAN_APIKEY,
    network: 'mainnet'
})

/**
 * total supply increase over time
 */
 test('[09|01] Get Total Supply of Ether', async function(t) {
    const total_supply = await stats.getTotalEtherSupply()

    t.ok(total_supply)
})

test('[09|02] Get Ether Last Price', async function(t) {
    const eth_price = await stats.getEtherLastPrice()

    t.ok(eth_price)
    t.isNot(Object.keys(eth_price).indexOf('ethbtc'), -1)
})

test('[09|03] Get Ethereum Nodes Size', async function(t) {
    const nodes = await stats.getNodeCount()

    t.ok(nodes)
    t.is(nodes.length, 24)
})
