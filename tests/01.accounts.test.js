require('dotenv').config()

const test = require('tape');
const _etherscan = require('../index');
const { accounts } = new _etherscan({
    token: process.env.ETHERSCAN_APIKEY,
    network: 'mainnet'
})

test('[01|01] Get Ether Balance for a Single Address', async function(t) {
    const balance = await accounts.getAccountBalance('0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae')
    t.ok(balance)
    t.equal(typeof balance, 'string')
    t.end()
})

test('[01|02] Get Ether Balance for Multiple Addresses in a Single Call', async function(t) {
    const addresses = [ 
        '0xddbd2b932c763ba5b1b7ae3b362eac3e8d40121a',
        '0x63a9975ba31b0b9626b34300f7f627147df1f526',
        '0x198ef1ec325a96cc354c7266a038be8b5c558f67'
    ];
    
    const balances = await accounts.getAccountBalances(addresses)
    t.ok(balances)
    t.equal(balances.length, 3)
    t.end()
})

test('[01|03] Get a list of \'Normal\' Transactions By Address', async function(t) {
    const transactions = await accounts.getTransactions('0xc5102fE9359FD9a28f877a67E36B0F050d81a3CC', {
        startblock: 0,
        endblock: 99999999,
        page:1,
        offset:10
    })

    t.ok(transactions)
    t.equal(transactions.length, 10)
    t.end()
})

test('[01|04] Get a list of \'Internal\' Transactions by Address', async function(t) {
    const transactions = await accounts.getInternalTransactions('0x2c1ba59d6f58433fb1eaee7d20b26ed83bda51a3', {
        startblock: 0,
        endblock: 2702578,
        page: 1,
        offset: 10
    })

    t.ok(transactions)
    t.equal(transactions.length, 10)
    t.end()
})

test('[01|05] Get \'Internal Transactions\' by Transaction Hash', async function(t) {
    const transactions = await accounts.getInternalTransactionsByHash('0x40eb908387324f2b575b4879cd9d7188f69c8fc9d87c901b9e2daaea4b442170')

    t.ok(transactions)
    t.equal(transactions.length, 1)
    t.end()
})

test('[01|06] Get "Internal Transactions" by Block Range', async function(t) {
    const transactions = await accounts.getInternalTransactionsByBlockRange({
        startblock: 13481773,
        endblock: 13491773,
        page: 1,
        offset: 10
    })

    t.ok(transactions)
    t.equal(transactions.length, 10)

    const tx = transactions.shift()

    t.equal(tx.blockNumber, '13481773')
    t.equal(tx.timeStamp, '1635100060')
    t.end()
})

test('[01|07] Get a list of \'ERC-20 Token Transfer Events\' by Address', async function(t) {
    const address = '0x4e83362442b8d1bec281594cea3050c8eb01311c';
    const contractaddress = '0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2';
    const transfers = await accounts.getERC20TokenTransferEventsByAddress(
        address,
        contractaddress,
        {
            startblock: 0,
            endblock: 27025780,
            page: 1,
            offset: 100
        }
    );

    t.ok(transfers)
    t.equal(transfers.length, 100)

    const tx = transfers.shift()

    t.equal(tx.blockNumber, '2228258')
    t.equal(tx.from, address);
    t.end()
})

test('[01|08] Get a list of \'ERC-721 Token Transfer Events\' by Address', async function(t) {
    const address = '0x6975be450864c02b4613023c2152ee0743572325';
    const contractaddress = '0x06012c8cf97bead5deae237070f9587f8e7a266d';
    const transfers = await accounts.getERC721TokenTransferEventsByAddress(
        address,
        contractaddress,
        {
            startblock: 0,
            endblock: 27025780,
            page: 1,
            offset: 100
        }
    );

    t.ok(transfers)
    t.equal(transfers.length, 100)

    const tx = transfers.shift()

    t.equal(tx.blockNumber, '4708120')
    t.equal(tx.to, address);
    t.end()
})

test('[01|09] Get list of Blocks Mined by Address', async function(t) {
    const address = '0x9dd134d14d1e65f84b706d6f205cd5b1cd03a46b';
    // can also call getMinedBlocks
    const blocks = await accounts.getMinedBlocks(address, {
        page: 1,
        offset: 10
    })

    const compare = {
        blockNumber: '3462296',
        timeStamp: '1491118514',
        blockReward: '5194770940000000000'
    }
    
    t.ok(blocks)
    t.equal(blocks.length, 10)

    const block = blocks.shift()

    t.deepEquals(block, compare)
    t.end()
})
