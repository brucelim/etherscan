require('dotenv').config()

const test = require('tape');
const _etherscan = require('../index');
const { gastracker, web3 } = new _etherscan({
    token: process.env.ETHERSCAN_APIKEY,
    network: 'mainnet'
})


test('[08|01] Get Estimation of Confirmation Time', async function(t) {
    const confirmation_time = await gastracker.getConfirmationTime(2000000000)

    t.ok(confirmation_time)
    t.is(typeof confirmation_time, 'string')
    t.is(confirmation_time, '3615')
})

test('[08|02] Get Gas Oracle', async function(t) {
    const gasprice = await gastracker.getGasOracle()

    t.ok(gasprice)
    t.is(typeof gasprice, 'object')
})
