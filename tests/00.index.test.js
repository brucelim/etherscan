require('dotenv').config()

const test = require('tape');
const _etherscan = require('../index');
const etherscan = new _etherscan({
    token: process.env.ETHERSCAN_APIKEY,
    network: 'mainnet'
})


test('[00] etherscan module setup', (t) => {
    t.equals(typeof _etherscan, 'function')
    t.equals(typeof etherscan, 'object')
    t.ok(etherscan.web3)
    t.ok(etherscan.accounts)
    t.ok(etherscan.contracts)
    t.ok(etherscan.blocks)
    t.ok(etherscan.logs)
    t.ok(etherscan.proxy)
    t.ok(etherscan.tokens)
    t.ok(etherscan.gastracker)
    t.ok(etherscan.stats)

    t.end()
})

