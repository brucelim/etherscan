require('dotenv').config()

const test = require('tape');
const _etherscan = require('../index');
const { contracts } = new _etherscan({
    token: process.env.ETHERSCAN_APIKEY,
    network: 'mainnet'
})


test('[02|01] Get Contract ABI for Verified Contract Source Codes', async function(t) {
    const abi = await contracts.getContractAbi('0xBB9bc244D798123fDe783fCc1C72d3Bb8C189413')
    t.ok(abi)

    const proposals = abi.shift();
    t.equal(proposals.name, 'proposals');
    t.equal(proposals.type, 'function');
    t.end()
})

test('[02|02] Get Contract Source Code for Verified Contract Source Codes', async function(t) {
    const response = await contracts.getContractSource('0xBB9bc244D798123fDe783fCc1C72d3Bb8C189413');
    t.ok(response)

    const source = response.shift()

    t.equal(source.ContractName, 'DAO')
    t.end()
})

test('[02|03] Get Contract Creator and Creation Tx Hash', async function(t) {
    const addresses = [
        '0xB83c27805aAcA5C7082eB45C868d955Cf04C337F',
        '0x68b3465833fb72A70ecDF485E0e4C7bD8665Fc45'
    ]
    const compare = {
        contractAddress: '0x68b3465833fb72a70ecdf485e0e4c7bd8665fc45',
        contractCreator: '0x6c9fc64a53c1b71fb3f9af64d1ae3a4931a5f4e9',
        txHash: '0x7299cca7203f60a831756e043f4c2ccb0ee6cb7cf8aed8420f0ae99a16883a2b'
    }
    const response = await contracts.getContractCreator(addresses);
    const item = response.shift()
    t.deepEquals(item, compare)
    t.end()
})
